cd 
cd testsimu/res
mkdir genome -p
cd genome
wget ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
mv GCF_000005845.2_ASM584v2_genomic.fna.gz ecoli_fasta.gz
gunzip -k ecoli_fasta.gz 
echo "Secuencia descargada"
cd
cd testsimu
mkdir -p out/fastqc
mkdir -p out/cutadapt
mkdir -p log/cutadapt
mkdir -p out/multiqc
mkdir -p out/multiqc/multiqc_data
mkdir -p res/genome/star_index
cd
cd testsimu
for sid in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed "s:data/::" | sort | uniq)
do
    bash scripts/analyse_sample.sh $sid
done; 
echo "Running multiQC..."
cd
cd testsimu
multiqc -o out/multiqc $WD
echo "Fin."
